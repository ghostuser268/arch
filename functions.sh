baseInstallation(){
	clear
	while true;do
		fdisk -l | awk '/Disk \/dev\// {print $0}'
		echo -ne "\n\nPartion device [E.g /dev/sda]: ";read device

		echo -ne "\n\nManual Partioning? [y/n] ";read partitioningType
		if [[ $partitioningType == 'n' ]];then
			echo -n "Efi size[ Eg. +500M ]: "; read efiSize
			echo -n "Root size[ Eg. +200G ]: "; read rootSize
			echo -n "Create swap?[y/n] ";read choice
			if [[ $choice == 'y' || $choice == '' || $choice == 'Y' ]];then echo -n "Swap size[ E.g +5G ]:";read swapSize ;fi
			
		fi
		echo -ne "\n\nUpdate mirror list? [y/n] ";read mirrorlist
		if [[ $mirrorlist == "Y" || $mirrorlist == 'y' || $mirrorlist == '' ]];then
			clear; echo "Fetching mirrorlist locations..."
			curl -s https://archlinux.org/mirrorlist/ | awk -F \> '/option value/{print $2}' | awk -F \< '{print $1}'| less
			echo -e "\n\n-_-_-_-_-_-_-_-_-_-_-_-_-_"
			echo -n "Country: ";read country
		fi


	clear
	echo -en "Variables:\n\tDevice to be partitioned: ${device}" 

	if [[ $partitioningType == 'y' ]];then echo -e "\n\tManual Partioning: True";else echo -e "\n\tAutomatic Partioning: True";fi
	if [[ $partitioningType == 'n' ]];then 
		echo -e "\n\tEfi Size: ${efiSize}\n\tRoot Size: ${rootSize}"
		if [[ $swapSize != '' ]];then echo -e "\tSwap Size: ${swapSize}";fi
	fi

	if [[ $mirrorlist != 'n' ]];then echo -e "\tUpdate mirrorlist: ${mirrorlist} | Country: ${country}";fi

	echo -ne "\n\tContinue? [y/n]";read choice
	if [[ $choice == 'y' || $choice == 'Y' || $choice == '' ]];then break;fi
	done


	#Partion Disks
	if [[ $partitioningType == "" || $partitioningType == 'y' || $partitioningType == 'Y' ]];then
		fdisk -W always $device
	elif [[ $partitioningType == 'n' ]];then
		case $choice in
			y) 
			( 
			echo g
			echo n;echo "";echo "";echo "${efiSize}";echo t;echo 1
			echo n;echo "";echo "";echo "${swapSize}";echo t;echo 2;echo swap
			echo n;echo "";echo "";echo "${rootSize}"
			echo w
			) | fdisk -W always $device ;;
			n) 
			(
			echo g
			echo n;echo "";echo "";echo "${efiSize}";echo t;echo 1
			echo n;echo "";echo "";echo "${swapSize}";echo t;echo 2;echo swap
			echo n;echo "";echo "";echo "${rootSize}"
			echo w
			) | fdisk -W always $device ;;

		esac
	fi
	
	efiPartition=$(fdisk -l ${device} | awk '/EFI System/{print $1}')		
	rootPartition=$(fdisk -l ${device} | awk '/Linux filesystem/{print $1}')	
	swapPartition=$(fdisk -l ${device} | awk '/Linux swap/{print $1}') 	

	#Format partitions
	mkfs.fat -F32 $efiPartition 
	mkfs.ext4 $rootPartition 
	if [[ $swapPartition != '' ]];then mkswap $swapPartition ;fi

	#Mount
	mount $rootPartition /mnt
	mkdir /mnt/boot
	mount $efiPartition /mnt/boot
	if [[ $swapPartition != '' ]];then swapon $swapPartition ;fi

	#Update MirrorList
	if [[ $country != '' ]];then
		reflector --country $country --latest 10 --save /etc/pacman.d/mirrorlist
	fi

	sed -i -e '37s/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf

	#Packages
	pacstrap /mnt $basePackages

	##Generate Fs
	genfstab -U /mnt >> /mnt/etc/fstab

	cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/
	cp -r /root/arch /mnt/root
	arch-chroot /mnt /root/arch/setup.sh 'root'
}

rootSetup(){
	#User Input
	while true;do
		ls /usr/share/zoneinfo/
		echo -n 'Region: '; read userRegion
		ls /usr/share/zoneinfo/$userRegion/
		echo -n 'City: '; read userCity
		echo -n "Hostname: ";read hostName
		echo -n "Cpu[amd/intel]:";read cpu
		echo "Only for single Gpu..."
		echo -n "Gpu[amd/intel/nvidia]:";read gpu
		echo -n "Root password: ";read rootPassword
		echo -n "Create new user? [y/n]";read newUser
		if [[ $newUser == 'y' || $newUser == 'Y' || $newUser == '' ]];then
			echo -n "Username: ";read userName;echo -n "${userName} password: ";read userPassword;
			echo -n "Add ${userName} to sudoers file? *Also Disables pass [y/n]";read sudoChoice
			#if $sudoChoice == 'y' || $sudoChoice == 'Y' || $sudoChoice == '' ;  then;fi
		fi 
		clear
		echo -e "Current variables:\n\tRegian:${userRegion}\n\tCity:${userCity}\n\tHostname:${hostName}\n\tCpu type:${cpu}\n\tGpu type:${gpu}\n\tRoot Password:${rootPassword}\n\t"
		if [[ $newuser != '' ]];then echo -e "New User:${userName}\n\tUser Password:${userPassword}" ;fi
		echo -n "Continue?[y/n]: ";read choice

		if [[ $choice == 'y' || $choice == 'Y' || $choice == '' ]];then break;fi
	done
	
	#Location/time/language
	ln -sf /usr/share/zoneinfo/$userRegion/$userCity /etc/localtime
	hwclock --systohc
	sed -i -e '177s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen 
	locale-gen
	echo "LANG=en_US.UTF-8" > /etc/locale.conf

	#Host
	echo $hostName > /etc/hostname 
	echo -e "127.0.0.1	localhost\n::1		localhost\n127.0.1.1	$hostName.localdomain	$hostName" > /etc/hosts
	(echo $rootPassword; echo $rootPassword) | passwd; 
	
	#pacman
	sed -i -e '37s/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf

	#Cpu
	if [[ $cpu == 'amd' ]];then pacman -S --noconfirm amd-ucode ;elif [[ $cpu == 'intel' ]];then pacman -S --noconfirm intel-ucode;fi

	#Gpu
	if [[ $gpu == 'amd' ]];then
		pacman -S --noconfirm $cpuTypeAMD 
	elif [[ $gpu == 'intel' ]];then
		pacman -S --noconfirm $cpuTypeIntel
	#elif [[ $gpu == 'nvidia' ]];then
		#pacman -S --noconfirm $cpuTypeNvidia
	fi

	#Root Packages
	pacman -S --noconfirm $rootPackages
	
	#Add User
	useradd -mG wheel $userName
	( echo $userPassword ; echo $userPassword ) | passwd $userName
	if [[ $sudoChoice == 'y' || $sudoChoice == 'Y' || $sudoChoice == '' ]];then sed -i -e '85s/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers ;fi


	#Bootloader => GRUB
	grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB 
	grub-mkconfig -o /boot/grub/grub.cfg

	#Misc
	systemctl enable NetworkManager
	systemctl enable sshd 
	
	mv /root/arch /home/${userName}/
	chown -R $userName /home/${userName}/arch
	echo -e "=>umount -R /mnt\nRestart into new user "

}

wmSetup(){

	echo "Is wifi connected? [y/n]";read choice
	if [[ $choice == 'n' ]];then exit;fi

	sudo pacman -S --noconfirm $x11Packages
	if [[ ! -d ~/personal/suckless ]];then mkdir -p ~/personal/suckless ;fi
	cd ~/personal/suckless
	
	for i in ${sucklessPackages[@]};do
		git clone $i
	done

	for i in ~/personal/suckless/* ;do
		cd $i
		sudo make clean install
	done

	echo -e "until dwm;do\n\tsleep 1\ndone" > ~/.xinitrc
}

