basePackages="base linux linux-firmware vim openssh reflector"

rootPackages="networkmanager grub efibootmgr base-devel man-db cmake ntfs-3g bc fzf tmux pass coreutils git wget imagemagick ranger ueberzug yajl python feh exa xorg-xinput xorg-xset"  
gpuTypeAMD="mesa mesa-vdpau xf86-video-amdgpu vulkan-radeon libva-mesa-driver"
gpuTypeIntel="mesa xf86-video-intel vulkan-intel"

x11Packages="xorg-xsetroot xorg-xinput xorg-server xorg-xinit libxft xorg-font-util xorg-fonts-100dpi xorg-fonts-75dpi adobe-source-code-pro-fonts"

surl='https://git.suckless.org'
sucklessPackages=("${surl}/dwm" "${surl}/st")








































