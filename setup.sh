#!/bin/sh

if [[ $(whoami) == 'root' ]];then
	source /root/arch/packages.sh
	source /root/arch/functions.sh
elif [[ $(whoami) != 'root' ]];then
	source ${HOME}/arch/packages.sh
	source ${HOME}/arch/functions.sh
fi


if [[ $1 == 'root' ]];then rootSetup;exit; fi


clear
echo "-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"
echo "_-								-_"
echo "_-	  Mistakes in variables will break setup!		-_"
echo "_-								-_"
echo "_-	       Default choice is set to [y].   	 	 	-_"
echo "_-								-_"
echo "_-   	     Partioning will be done with fdisk.         	-_"
echo "_-								-_"
echo "_-		  No support for nvidia.	 	  	-_"
echo "_-								-_"
echo "_-      		No support for dual graphics.		 	-_"
echo "_-								-_"
echo "-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"

echo -en "\n\nOptions:\n\t1) Base Installation\n\t2) WM Setup\n\tChoice: "; read option

case $option in 
	1) baseInstallation ;;
	2) wmSetup ;;
	*) echo -e "\nChoice not specified! E.g) 1" ;;
esac



